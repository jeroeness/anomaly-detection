
# Discretize the sensor data using any of the methods discussed in class. Explain why you choose this method and why the
# obtained discretization makes sense. Visualize the discretization.
# Apply any of the sequential data mining methods (N-grams, Sequence alignment with kNN, …) to sliding windows with a
# length of your choosing in order to find anomalies. Whenever an observed N-gram’s probability is too small, or the
# sequential data is too distant from any training sequence, raise an alarm. Set your thresholds sensibly. What kind of
# anomalies can you detect using the sequential model? Which sensors can be modeled effectively?

import pandas as pd
from nltk import ngrams
import numpy as np
from sklearn.metrics import confusion_matrix, f1_score
import matplotlib.pyplot as plt

train_1 = pd.read_csv("data/BATADAL_training_1.csv", header=0)
train_2 = pd.read_csv("data/BATADAL_training_2.csv", header=0)

# Rename the columns in train_2 to remove the leading space
rename_dict = {}
for col in train_2.columns:
    rename_dict[col] = col.lstrip()
train_2 = train_2.rename(index=str, columns=rename_dict)

# We drop the DATETIME and ATT_FLAG column
train_1 = train_1.drop(["DATETIME", "ATT_FLAG"], axis=1)
train_2_labels = ((train_2["ATT_FLAG"] == -999).astype(int) - 1) ** 2
train_2 = train_2.drop(["DATETIME", "ATT_FLAG"], axis=1)

# We select all columns that do not hold binary data.
# Note that we leave out F_PU3, F_PU5 and F_PU9 as well, they are always zero
bin_columns = ["L_T1", "L_T2", "L_T3", "L_T4", "L_T5", "L_T6", "L_T7", "F_PU1", "F_PU2", "F_PU4", "F_PU6", "F_PU7",
               "F_PU8", "F_PU10", "F_PU11", "F_V2", "P_J280", "P_J269", "P_J300", "P_J256", "P_J289", "P_J415",
               "P_J302", "P_J306", "P_J307", "P_J317", "P_J14", "P_J422"]


def bin_data(bin_num):
    # Bin each column into equal-width bins. This is important as we would expect anomalies to be on the edges.
    # If an equal number of points were used per bin, so it is not fixed width, then we
    # would lose sight of the anomalies.
    for col in bin_columns:
        train_1[col + "_binned"], bins = pd.cut(train_1[col], bin_num, labels=False, retbins=True)
        train_2[col + "_binned"] = pd.cut(train_2[col], bins, labels=False, include_lowest=True)
        train_2[col + "_binned"] = train_2[col + "_binned"].fillna(0)

    # We will apply the methods to binary and binned columns, so we remove non-binned and
    # non-binary columns, as well as
    # columns that are always the same (S_PU1, S_PU3, S_PU4, S_PU9, F_PU3, F_PU5 and F_PU9)
    train_1_binned = train_1.drop(columns=bin_columns, axis=1)
    train_1_binned = train_1_binned.drop(columns=["S_PU1", "S_PU3", "S_PU4", "S_PU9", "F_PU3", "F_PU5", "F_PU9"])
    train_2_binned = train_2.drop(columns=bin_columns, axis=1)
    train_2_binned = train_2_binned.drop(columns=["S_PU1", "S_PU3", "S_PU4", "S_PU9", "F_PU3", "F_PU5", "F_PU9"])
    return train_1_binned, train_2_binned


def apply_grams(gram_num, train_1_binned, train_2_binned):
    # Let's 'train' on train_1 and test on train_2
    # For each column, we create the ngrams, aggregate and normalize
    train_1_grams = {}
    for col in train_1_binned.columns:
        train_1_grams[col] = {}
        # Create the ngrams
        grams = list(ngrams(train_1_binned[col], gram_num))
        s = len(grams)
        for gram in grams:
            # If it is new, add it
            if gram not in train_1_grams[col]:
                train_1_grams[col][gram] = 0
            # Increment and normalize
            train_1_grams[col][gram] += 1 / s

    # Test using train_2
    marked = np.zeros(train_2_binned.shape)
    for col_n, col in enumerate(train_2_binned.columns):
        # Create the ngrams
        grams = list(ngrams(train_2_binned[col], gram_num))
        for i, gram in enumerate(grams):
            # If the current ngram does not exist in the ngrams of train_1, we have an anomaly
            if gram not in train_1_grams[col]:
                # We mark that we found an anomaly in this sensor at this point in time
                marked[i, col_n] += 1

    return (marked.sum(axis=1) > 1) * 1  # Multiply by 1 to transform to binary instead of boolean


# Try each combination to see which performs best
bins_numbers = [3, 4, 5, 6, 7, 8, 9]
grams_numbers = [2, 3, 4, 5, 6, 7]
for bin_number in bins_numbers:
    for gram_number in grams_numbers:
        train_1_binned, train_2_binned = bin_data(bin_number)
        prediction = apply_grams(gram_number, train_1_binned, train_2_binned)
        # Print the confusion matrix, in the form [TN, FP, FN, TP]
        # print("bins: %d, grams: %d, scores: %s" %
        #       (bin_number, gram_number, confusion_matrix(train_2_labels, prediction).ravel()))
        # print("bins: %d, grams: %d, F-1 score: %.4f" %
        #       (bin_number, gram_number, f1_score(train_2_labels, prediction)))

# We see that 4 bins and 4-grams work best, so we will further inspect those
# First visualize the discretization
# It does not matter whether we show this for train_1 or train_2
bin_num = 3
gram_num = 4
train_1_binned, train_2_binned = bin_data(bin_num)
for col in bin_columns:
    # print("Comparison between binned and unbinned data in column %s with %d bins" % (col, bin_num))
    plt.figure()
    x = np.arange(1, len(train_2[col])+1)
    plt.plot(x, train_2[col], alpha=0.5, c="blue")
    plt.plot(x, train_2[col + "_binned"], alpha=0.5, c="red")
    plt.title("Inspect binning in column %s with %d bins" % (col, bin_num))
    plt.legend(["Regular", "Discrete"])
    plt.savefig("plots/train_2_bin_comparison_%s_%d.png" % (col, bin_num))
    plt.close()

# Show the overlap between detected anomalies and actual anomalies
prediction = apply_grams(gram_num, train_1_binned, train_2_binned)
plt.figure()
x = np.arange(1, len(train_2_labels)+1)
plt.plot(x, train_2_labels, c="red")
plt.plot(x, prediction, c="blue", alpha=0.5)
plt.legend(["Truth", "Prediction"])
plt.title("Detected anomalies in training_2 using %d-grams with %d bins" % (gram_num, bin_num))
plt.savefig("plots/anomaly_detection_%d_bins_%d_grams_train2.png" % (gram_num, bin_num))
plt.close()
print("bins: %d, grams: %d, scores: %s, F1: %.4f" %
      (bin_num, gram_num, confusion_matrix(train_2_labels, prediction).ravel(), f1_score(train_2_labels, prediction)))

# Check using the extended labels
extended_labels = np.array(train_2_labels)
extended_labels[1728:1777] = 1
extended_labels[2028:2051] = 1
extended_labels[2828:2921] = 1
extended_labels[3498:3557] = 1
extended_labels[3728:3821] = 1
extended_labels[3928:4037] = 1

plt.figure()
plt.plot(x, train_2_labels, c='red')
plt.plot(x, extended_labels, c='orange')
plt.plot(x, prediction, c='blue', alpha=0.5)
plt.legend(["Truth", "Extended", "Prediction"])
plt.title("Detected anomalies in training_2 using %d-grams with %d bins, with extended labels" % (gram_num, bin_num))
# plt.savefig("plots/anomaly_detection_ngrams_extended.png")
plt.close()
print("bins: %d, grams: %d, scores: %s, F1: %.4f" %
      (bin_num, gram_num, confusion_matrix(extended_labels, prediction).ravel(), f1_score(extended_labels, prediction)))
