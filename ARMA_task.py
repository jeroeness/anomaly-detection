from __future__ import print_function
import numpy as np
from scipy import stats
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
from sklearn.metrics import confusion_matrix

residuals_list = dict()

train_2 = pd.read_csv("data/BATADAL_training_2.csv", header=0)
### Delete the datefield of the data
del train_2["DATETIME"]

### This is a hack which turns the ATT_FLAG column in a binary 1/0 domain.
train_2_labels = ((train_2[" ATT_FLAG"] == -999).astype(int) - 1) ** 2


### Select interesting sensors and iterate over them to perform ARMA analysis
selected_cols = [" P_J14", " P_J317", " S_PU11", " S_PU6", " L_T1"]
for col in selected_cols:
    data = train_2[col]

    ### plot auto correlation and partial autocorrelation function
    fig = plt.figure(figsize=(12,8))
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(data.values.squeeze(), lags=50, ax=ax1, title="Autocorrelation: " + col)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(data, lags=50, ax=ax2, title="Partial Autocorrelation: " + col)
    plt.savefig("plots/acf_pacf"+col+".jpg")
    fig.show()


### The order for the ARMA model found by looking at the ACF and PACF
respective_p_and_q = [(1, 5), (3, 3), (7, 1), (1,1), (3, 9)]

### Initial predictions, all set to zero
prediction = [0] * len(train_2_labels)

for i in range(len(selected_cols)):
    col = selected_cols[i]
    data = train_2[col]

    ### Select p and q according to observed ACF and PACF
    p = respective_p_and_q[i][0]
    q = respective_p_and_q[i][1]

    ### Fit arma model
    arma = sm.tsa.ARMA(data, (p,q)).fit(disp=False)

    ### Show properties of fitted model
    print(arma.params)
    print(arma.aic, arma.bic, arma.hqic)

    ### Define thresholds
    resid = arma.resid
    mean = np.mean(resid)
    std = np.std(resid)*3
    upperbound = [mean + std]* len (train_2)
    lowerbound = [mean - std] * len(train_2)

    for k in range(len(resid)):
        prediction[k] = 1 if abs(resid[k]) > mean + std or prediction[k] == 1 else 0

    ### Plot ARMA residuals
    fig = plt.figure(figsize=(12,8))
    ax = fig.add_subplot(111)
    ax = arma.resid.plot(ax=ax)
    ax.plot(train_2_labels)
    ax.plot(upperbound, linestyle="--", color="red")
    ax.plot(lowerbound, linestyle="--", color="red")
    plt.title("residual: " + col)
    plt.legend(["Residual","Attack label", "Threshold"])
    plt.savefig("plots/resid"+col[1:]+".jpg")
    fig.show()


mat = confusion_matrix(train_2_labels, prediction)
print(mat)