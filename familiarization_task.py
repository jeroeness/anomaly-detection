# Load the BATADAL sensor data (start with training data 1, optimize using training data 2, test with test data) into
# your favorite analysis platform (R, Matlab, Python, Weka, KNIME, ...) and understand the data.
# Answer the following questions:
# - What kinds of signals are there?
# - Are the signals correlated? Do they show cyclic behavior?
# - Is predicting the next value in a series easy or hard? Use any method from class.
# Visualize these types, the presence or absence of correlation, and the performance of prediction.
import pandas
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm

train_1 = pandas.read_csv("data/BATADAL_training_1.csv", header=0)
train_2 = pandas.read_csv("data/BATADAL_training_2.csv", header=0)
test = pandas.read_csv("data/BATADAL_test.csv", header=0)

# Check correlation
train_1_correlations = train_1.corr()
train_2_correlations = train_2.corr()
test_correlations = test.corr()

# Plot ACF, PACF
fig = plt.figure(figsize=(12,8))
ax1 = fig.add_subplot(211)
fig = sm.graphics.tsa.plot_acf(train_1["P_J317"].values.squeeze(), lags=50, ax=ax1, title="Autocorrelation: P_J317")
ax2 = fig.add_subplot(212)
fig = sm.graphics.tsa.plot_pacf(train_1["P_J317"].values.squeeze(), lags=50, ax=ax2, title="Partial Autocorrelation: P_J317")
plt.savefig("plots/fam_acf_pacf_P_J317.png")
fig.show()
plt.close()

# Plot and save correlations
# Plot/save for train_1
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(train_1_correlations)
fig.colorbar(cax)
ax.set_yticks(np.arange(0, len(train_1.columns.values.tolist())))
ax.set_yticklabels(train_1.columns.values.tolist())
plt.title("Correlation matrix of training dataset 1")
plt.savefig("plots/train_1_correlations.png")
plt.close()

# Plot/save for train_2
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(train_2_correlations)
fig.colorbar(cax)
ax.set_yticks(np.arange(0, len(train_2.columns.values.tolist())))
ax.set_yticklabels(train_2.columns.values.tolist())
plt.title("Correlation matrix of training dataset 2")
plt.savefig("plots/train_2_correlations.png")
plt.close()

# Plot/save for test
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(test_correlations)
fig.colorbar(cax)
ax.set_yticks(np.arange(0, len(test.columns.values.tolist())))
ax.set_yticklabels(test.columns.values.tolist())
plt.title("Correlation matrix of test dataset 1")
plt.savefig("plots/test_correlations.png")
plt.close()

# For each column except the first, plot the rolling average for data of a week (24*7 hours) and the autocorrelation
for col in train_1.columns:
    if col != "DATETIME":
        periodicity = 6
        train_1[[col]].rolling(periodicity).mean().plot()
        plt.xlim((0, 96))
        plt.title("Rolling average of %d hours for sensor %s" % (periodicity, col))
        # plt.show()
        plt.close()
