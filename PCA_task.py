# Perform PCA-based anomaly detection on the signal data. Set the threshold on training data to a value that results in
# few false positives on the training data. Plot the PCA residuals in one signal. Do you see large abnormalities in the
# training data? Can you explain why these occur? It is best to remove such abnormalities from the training data since
# you only want to model normal behavior. What kind of anomalies can you detect using PCA?
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, f1_score

train_1 = pd.read_csv("data/BATADAL_training_1.csv", header=0)
train_2 = pd.read_csv("data/BATADAL_training_2.csv", header=0)
test = pd.read_csv("data/BATADAL_test.csv", header=0)

# Drop the DATETIME column, this is in String format so it results in errors
# Also drop the ATT_FLAG column, it is not useful in PCA as we need to compare the data from train_1 and train_2 to
# the data in test
train_1 = train_1.drop(["DATETIME", "ATT_FLAG"], axis=1)
# In the case of training data 2, we change the ATT_FLAG to binary, instead of -999 or 1
train_2_labels = ((train_2[" ATT_FLAG"] == -999).astype(int) - 1) ** 2
train_2 = train_2.drop(["DATETIME", " ATT_FLAG"], axis=1)  # Yes the extra space is correct...
# The test data does not contain ATT_FLAG so we only remove DATETIME
test = test.drop(["DATETIME"], axis=1)

# Scale data
scaler = StandardScaler()
scaler.fit(train_1)
x_scaled_1 = scaler.transform(train_1)
scaler.fit(train_2)
x_scaled_2 = scaler.transform(train_2)


def apply_pca(train, test, n_components, std_threshold):
    pca = PCA(n_components=n_components, svd_solver='full', whiten=True)  # whiten to scale components to unit variance

    # We 'train' the PCA 'classifier'
    pca.fit(train)

    # We then test on train_2, reducing the data to n_components
    x_test_pca = pca.transform(test)
    x_test_pca = pd.DataFrame(data=x_test_pca, index=train_2.index)

    # We can see how much of the variance is explained by the components, by executing the two lines below
    # print(pca.explained_variance_ratio_)
    # print(np.sum(pca.explained_variance_ratio_))

    # We then calculate the inverse, to see how much we can still predict
    x_test_pca_inverse = pca.inverse_transform(x_test_pca)
    x_test_pca_inverse = pd.DataFrame(data=x_test_pca_inverse, index=train_2.index)

    # To see the difference between malignant en benign cases, we can plot the first two components
    # x_benign = x_test_pca[train_2_labels == 0]
    # x_malignant = x_test_pca[train_2_labels == 1]
    # ax1 = x_benign.plot(kind="scatter", x=0, y=1, c="blue")
    # x_malignant.plot(kind="scatter", x=0, y=1, c="red", ax=ax1)
    # plt.title("Most significant components")
    # plt.legend(["Benign", "Malignant"])
    # plt.savefig("plots/anomaly_detection_PCA_2_components.png")
    # plt.close()

    # We calculate the information loss by taking the squared distance between the original and reproduced data
    # In PCA, the components that explain the littlest variance are left out. In the case of anomalies these components
    # do actually hold information. By calculating the distance, we expect greater distances for anomalies.
    loss = np.sum((np.array(x_scaled_2) - np.array(x_test_pca_inverse)) ** 2, axis=1)
    loss = pd.Series(data=loss, index=train_2.index)

    # We then normalize all loss data so all points lie between 0 and 1, with the latter being anomalies.
    loss = (loss - np.min(loss)) / (np.max(loss) - np.min(loss))

    # Finally, we look at the labels and determine how many we got correct by transforming the loss into a binary
    # array based on a threshold
    threshold = std_threshold * np.std(loss)  # 2 times standard deviation is 67%, 3 times is 97.8%
    bin_loss = [(x > threshold) * 1 for x in loss]
    return loss, bin_loss


# Test multiple number of components
for n in range(2, 44):
    _, prediction = apply_pca(x_scaled_1, x_scaled_2, n, 2)
    # print("%d components, F-1 score: %.4f, scores: %s" %
    # (n, f1_score(train_2_labels, prediction), confusion_matrix(train_2_labels, prediction).ravel()))
    _, prediction = apply_pca(x_scaled_1, x_scaled_2, n, 3)
    # print("%d components, F-1 score: %.4f, scores: %s" %
    # (n, f1_score(train_2_labels, prediction), confusion_matrix(train_2_labels, prediction).ravel()))

# We see that 2 components and 3 times std actually produce the best results, so we use this
loss, bin_loss = apply_pca(x_scaled_1, x_scaled_2, 2, 3)

# To inspect the residuals, we can plot the loss
plt.figure()
plt.plot(loss)
plt.title("Loss using PCA")
plt.hlines(3 * np.std(loss), xmin=0, xmax=len(loss))
plt.legend(["Loss", "3x standard deviation"])
plt.savefig("plots/anomaly_detection_PCA_loss.png")
plt.close()

# To see where and when attacks were occurring and detected we can plot the labels and binary loss
plt.figure()
plt.plot(train_2_labels, c='red')
plt.plot(bin_loss, c='blue', alpha=0.5)
plt.legend(["Truth", "Prediction"])
plt.title("Detected anomalies in training_2 using PCA")
plt.savefig("plots/anomaly_detection_PCA_score.png")
plt.close()

# Check using the extended labels
extended_labels = np.array(train_2_labels)
extended_labels[1728:1777] = 1
extended_labels[2028:2051] = 1
extended_labels[2828:2921] = 1
extended_labels[3498:3557] = 1
extended_labels[3728:3821] = 1
extended_labels[3928:4037] = 1

plt.figure()
plt.plot(train_2_labels, c='red')
plt.plot(extended_labels, c='orange')
plt.plot(bin_loss, c='blue', alpha=0.5)
plt.legend(["Truth", "Extended", "Prediction"])
plt.title("Detected anomalies in training_2 using PCA, with extended labels")
plt.savefig("plots/anomaly_detection_PCA_extended.png")
plt.close()

print("%d components, F-1 score: %.4f, scores: %s" %
      (2, f1_score(extended_labels, bin_loss), confusion_matrix(extended_labels, bin_loss).ravel()))
